---
title: Linearly Constrained Register Allocation
author: Alyssa Rosenzweig (Collabora)
...

Linearly constrained register allocation (LCRA) is an algorithm for allocating registers for SIMD processors with many type sizes. Rather than describing an interference graph, it encodes _constraints_. That is, instead of considering graph colouring's test of:

$$k_i = k_j$$

LCRA generalizes to include alignments $a$ and components $c$, in order to represent vector data types bytewise:

$$a_i k_i + c_i \ne a_j k_j + c_j$$

These linear constraints form a generalization of graph colouring. Additionally, these constraints are augmented by constraints to "restrict" the range of nodes to avoid crossing word boundaries; these extra constraints reduce to modulo relations. LCRA thus enables per-byte tracking, useful for vector architectures with masking, as well as per-instruction tracking like graph colouring.

The resulting constraint set can be solved with a greedy solver.

LCRA features support for disjoint register classes, representing different isolated sections of the register file whose nodes never interfere.

Throughout the paper, LCRA assumes that node indices $i$ and offsets $c_i$ are integers, and further that alignments $a_i$ and word boundary $b$ are positive powers-of-two.

## Interference

The fundamental operation by a register allocator is adding interference between nodes $i$ and $j$. In graph colouring, this corresponds to adding edges to the interference graph. In LCRA, this corresponds to adding linear constraints.

First, if the classes of $i$ and $j$ differ, drop the interference constraint, as nodes from disjoint classes cannot interfere by definition.

Second, if $i = j$, drop the interference constraint, as a node cannot interfere with itself. If a node tried to interfere with itself
in the same component, this is spurious. Otherwise we have $a_i k_i + c_l \ne a_i k_i + c_r \Rightarrow c_l \ne c_r$. But we already assumed $c_l \ne c_r$; this condition is always satisfied, so
the constraint need not be added explicitly.

Provided the $i \ne j$ with the same class, proceed by the definition of interference:

$$a_l k_l + c_l \ne a_r k_r + c_r$$

$$a_l k_l + (c_l - c_r) \ne a_r k_r$$
$$a_l k_l \ne a_r k_r + (c_r - c_l)$$

$$k_l \ne \frac{a_r}{a_l} k_r + \frac{c_r - c_l}{a_l}$$
$$k_r \ne \frac{a_l}{a_r} k_l + \frac{c_l - c_r}{a_r}$$

If $a_r \ge a_l$, we have $\frac{a_r}{a_l} \in \mathbb{Z}$ (since $a_r, a_l$
powers-of-two). Thus, to solve with $k_l, k_r \in \mathbb{Z}$, we need $\frac{c_r - c_l}{a_l} \in \mathbb{Z}$. If this is not the case, eliminate the constraint. Otherwise, $a_r < a_l$, so we eliminate constraints where $\frac{c_l - c_r}{a_r} \notin \mathbb{Z}$

If the constraint has not been eliminated, store this new constraint.

## Range restriction

The next operation required of a user of a register allocator is "range restriction". In particular, due to alignment restrictions, we would like nodes to avoid crossing certain special word boundaries, occurring every $b$ bytes. We formalize this condition via rounding down as:

$$\lfloor \frac{a_i k_i}{b} \rfloor = \lfloor \frac{a_i k_i + l - 1}{b} \rfloor = \lfloor \frac{a_i k_i}{b} + \frac{l - 1}{b} \rfloor$$

Since both $a_i$ and $b$ are powers-of-two, with $b \geq a_i$, we have $d = \frac{b}{a_i}$ is itself a power-of-two and therefore an integer:

$$\lfloor \frac{k_i}{d} \rfloor = \lfloor \frac{k_i}{d} + \frac{l - 1}{b} \rfloor$$

We can rewrite as an inequality:

$$\lfloor \frac{k_i}{d} \rfloor \leq \frac{k_i}{d} + \frac{l - 1}{  b} < \lfloor \frac{k_i}{d} \rfloor + 1$$

We have $l, b \geq 1$ so $\frac{l - 1}{b}$ is non-negative, so the first half of the inequality comes for
free, reducing to:

$$\frac{k_i}{d} + \frac{l - 1}{b} < \lfloor \frac{k_i}{d} \rfloor + 1$$
$$\frac{k_i}{d} + \frac{l - 1}{b} - \lfloor \frac{k_i}{d} \rfloor < 1$$
$$\mathrm{frac} (\frac{k_i}{d}) + \frac{l - 1}{b} < 1$$
$$\mathrm{frac} (\frac{k_i}{d}) < 1 - \frac{l - 1}{b}$$
$$\frac{k_i \mod d}{d} < 1 - \frac{l - 1}{b}$$
$$\frac{k_i \mod d}{d} < \frac{b - l + 1}{b}$$
$$k_i \mod d < d \cdot \frac{b - l + 1}{b}$$
$$k_i \mod d < \frac{b}{a_i} \cdot \frac{b - l + 1}{b}$$
$$k_i \mod d < \frac{b - l + 1}{a_i}$$

Finally, note the left hand side is an integer, but the right hand side is not
necessarily an integer. We can round up the right hand side to ensure the final
constraint involves only integer arithmetic. Thus the final constraint we
impose and store is:

$$k_i \mod \frac{b}{a_i} < \lceil \frac{b - l + 1}{a_i} \rceil$$

## Solutions

Once LCRA constraints are generated via the above equations, a valid register allocation can be solved with a simple greedy solver. Proceed in $n$ steps for $n$ nodes. At step $i$, consider the constraints in $\{ k_0, ..., k_i \}$ and find the smallest $k_i$ satsifying all constraints. Proceed for all $k_i$.

Given a candidate values $(k_0, ..., k_i)$, linear constraint may be tested with bruteforce, iterating all constraints and checking if they are satisfied. By noticing the constant term of linear constraints is bounded, constraints for a given pair of nodes and coefficient may be stored as a bit field, such that we may rewrite the constraint $k_i \ne c_i k_i + b$ as $k_i - c_i k_i \ne b$, computing the difference directly and checking all $b$ values in the bitfield in parallel. This corresponds to a constant-factor speedup in the solver. 

The modulo constraints may be satisfied automatically with a modulo-aware iteration of $k$ values. Consider the modulo relation (where $P = \frac{b}{a_i}$ for $b$ the boundary and $Q$ the modulus):

$$k \mod P \leq Q$$

We can rewrite this as with $m, n \in \mathbb{Z}$:

$$k = mP + n$$
$$n < Q$$

Hence we use that form for $k$. In the solver, rather than iterating $k$ directly, instead iterate $m, n$, for which the modulo relation is satisfied, further eliminating useless tests of the linear constraints.

As for bounds of iteration, note:

$$k < k_{\mathrm{max}}$$
$$mP + n < k_{\mathrm{max}}$$
$$mP < k_{\mathrm{max}} - n$$
$$m < \frac{k_{\mathrm{max}} - n}{P}$$

Recall $0 \leq n \leq Q$, so we have:

$$m < \frac{k_{\mathrm{max}} - Q}{P} < ... < \frac{k_{\mathrm{max}}}{P}$$

Note for $k_{\mathrm{max}}, P$ powers-of-two with $k_{\mathrm{max}} \geq P$, $m$ is a power-of-two and therefore $m \in \mathbb{Z}$.

Further note $r_i = k_i a_i \Rightarrow k_{\mathrm{max}} = \frac{r_{\mathrm{max}}}{a_i}$. Given the maximum register $r_{\mathrm{max}}$, we can therefore calculate the maximum $k$ value and from there via the above relations the maximum $m$ value, successfully identifying the bounds.

## Spilling

During solving, if no possible value of $k_i$ solves all constraints simultaneous, it is necessary to spill a node. One simple heuristic is to consider only nodes of the same class of node $i$, and to opt to spill the node within that class with the most benefit given the constraints. Optimizing spilling with LCRA remains for future work.

## Conclusion

Linearly-constrained register allocation presents an alternative method for register allocation, ideal for vector (SIMD) architectures with complex type sizes and constraints and limited register space. The LCRA algorithm is implemented in the Panfrost compiler for Mali Midgard GPUs, with performance superior to standard graph colouring.
